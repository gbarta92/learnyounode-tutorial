var net = require('net');
var strftime = require('strftime');

var port = process.argv[2];

var time = function() {
    var d = new Date();
    return strftime('%Y-%m-%d %H:%M', d);
}

var server = net.createServer(function(socket) {
  socket.end(time() + '\n');
}).listen(port);
