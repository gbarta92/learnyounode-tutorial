var fs = require('fs');

var fileName = process.argv[2];

fs.readFile(fileName, 'utf8', function(err, data) {
  if(err)
    throw err;
  var lineByLine = data.split('\n');
  console.log(lineByLine.length - 1);
});
