var http = require('http');
var bl = require('bl');

const numUrls = 3;
var responses = [];

var getResponses = function(idx, callback) {
    http.get(process.argv[idx + 2], function(response) {
      response.pipe(bl(function(err, data) {
        callback(data.toString(), idx);
      }));
    });
}

var check = function() {
  for(var i = 0; i < responses.length; i++)
    if(!responses[i])
      return false;
  return true;
}

var printResponeses = function() {
  for(var i = 0; i < responses.length; i++)
    console.log(responses[i]);
}

for(var i = 0; i < process.argv.length - 2; i++)
  getResponses(i, function(data, index) {
    responses[index] = data;
    if(responses.length == numUrls)
      if(check())
        printResponeses();
  });
