var fs = require('fs');
var path = require('path');

module.exports = function(dir, ext, callback) {
  var fileList = [];
  fs.readdir(dir, function(err, data) {
      if(err) return callback(err);
      data.forEach(function(elem) {
        if(path.extname(elem).slice(1) === ext) {
          fileList.push(elem);
        }
      });
      callback(null, fileList);
  });
}
